Pod::Spec.new do |s|  
  s.name         = "XBrainSpeech"
  s.version      = "1.5.1"
  s.summary      = "XBrain speech framework"
  s.homepage     = "http://xbrain.io"
  s.license      = ''
  s.author       = { "Nicolas Mauri" => "nicolas.mauri@xbrain.io" }
  s.platform     = :ios, '8.0'
  s.requires_arc = true

  #### uncomment in development mode (i.e. pod locally referenced)
  #### comment out otherwise
  #s.source_files  = 'XBrainSpeech', 'XBrainSpeech/**/*.{h,m}'
  #s.public_header_files = 'XBrainSpeech/**/*.h'
  ####
  
  #### comment in development mode (i.e. pod locally referenced)
  #### uncomment otherwise
  s.source_files = 'XBrainSpeech/XBrainSpeech.framework/Headers/*.h'
  s.vendored_frameworks = 'XBrainSpeech/XBrainSpeech.framework'
  ####

  s.source = { :git => "https://" +ENV['BITBUCKET_USERNAME']+ "@bitbucket.org/xbrainsoft/internals-libraries-ios-xbrainspeech.git", :tag => s.version }

end  
