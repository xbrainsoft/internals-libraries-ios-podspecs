Pod::Spec.new do |s|  
  s.name         = "XBrainMedia"
  s.version      = "1.6.1"
  s.summary      = "XBrain media handling library"
  s.homepage     = "http://xbrain.io"
  s.license      = ''
  s.author       = { "Nicolas Mauri" => "nicolas.mauri@xbrain.io" }
  s.platform     = :ios, '8.0'
  s.requires_arc = true

  #### uncomment in development mode (i.e. pod locally referenced)
  #### comment out otherwise
  #s.source_files  = 'XBrainMedia', 'XBrainMedia/**/*.{h,m}'
  #s.public_header_files = 'XBrainMedia/**/*.h'
  ####
  
  #### comment in development mode (i.e. pod locally referenced)
  #### uncomment otherwise
  s.source_files = 'XBrainMedia/XBrainMedia.framework/Headers/*.h'
  s.vendored_frameworks = 'XBrainMedia/XBrainMedia.framework'
  ####

  s.source = { :git => "https://" +ENV['BITBUCKET_USERNAME']+ "@bitbucket.org/xbrainsoft/internals-libraries-ios-xbrainmedia.git", :tag => s.version }

  s.dependency 'XBrainClient', '~> 1.4'
  s.dependency 'AFNetworking', '~> 2.0'
  s.library = 'sqlite3'

end  
