Pod::Spec.new do |s|
  s.name         = "XBrainClient"
  s.version      = "1.5.0"
  s.summary      = "XBrain communication library"
  s.homepage     = "http://xbrain.io"
  s.license      = ''
  s.author       = { "Nicolas Mauri" => "nicolas.mauri@xbrain.io" }
  s.platform     = :ios, '8.0'
  s.requires_arc = true

  #### uncomment in development mode (i.e. pod locally referenced)
  #### comment out otherwise
  #s.source_files  = 'XBrainClient', 'XBrainClient/**/*.{h,m}'
  #s.public_header_files = 'XBrainClient/**/*.h'
  ####
  
  #### comment in development mode (i.e. pod locally referenced)
  #### uncomment otherwise
  s.source_files = 'XBrainClient/XBrainClient.framework/Headers/*.h'
  s.vendored_frameworks = 'XBrainClient/XBrainClient.framework'
  ####

  s.source = { :git => "https://" +ENV['BITBUCKET_USERNAME']+ "@bitbucket.org/xbrainsoft/internals-libraries-ios-xbrainclient.git", :tag => s.version }

  s.dependency 'SocketRocket', '~> 0.3.1-beta2'
  s.dependency 'ZipArchive', '~> 1.3'
  s.dependency 'ProtocolBuffers', '~> 1.9.2'

end  
